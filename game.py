# import the pygame module 
# have now made the first player disappear, have to add collision checks for the second one and the flow of game, along with score counter and speed increase 
import pygame 
import os  
import sys
import math
import config
# import pygame.locals for easier  
# access to key coordinates 
from pygame.locals import *
from config import *

pygame.init()

global p1_hit
p1_hit=0
global p2_hit
p2_hit=0

class Player1(object):  
    def __init__(self):
        """ The constructor of the class """
        self.img_b = pygame.image.load(os.path.join(img_path, 'player1.png'))
        self.image = pygame.transform.rotozoom(self.img_b, 0, 0.5)
        # the Player's position
        self.x = 0
        self.y = 0
        self.score=0
        self.chk=0

    def handle_keys(self):
        """ Handles Keys """
        key = pygame.key.get_pressed()
        dist = 5 # distance moved in 1 frame, try changing it to 5
        if key[pygame.K_DOWN]: # down key
            self.y += dist # move down
        elif key[pygame.K_UP]: # up key
            self.y -= dist # move up
        if key[pygame.K_RIGHT]: # right key
            self.x += dist # move right
        elif key[pygame.K_LEFT]: # left key
            self.x -= dist # move left
        if(self.x > (WIDTH-35)):
            self.x -= dist
        elif(self.x < 0):
            self.x += dist
        elif(self.y < 0):
            self.y += dist
        elif(self.y > (HEIGHT-35)):
            self.y -= dist

    def draw(self, surface):
        """ Draw on surface """
        # blit yourself at your current position
        surface.blit(self.image, (self.x, self.y))

class Player2(object):  
    def __init__(self):
        """ The constructor of the class """
        self.img_b = pygame.image.load(os.path.join(img_path, 'player2.png'))
        self.image = pygame.transform.rotozoom(self.img_b, 0, 1)
        # the Player's position
        self.x = 0
        self.y = 0
        self.score=0
        self.chk=0

    def handle_keys(self):
        """ Handles Keys """
        key = pygame.key.get_pressed()
        dist = 5 # distance moved in 1 frame, try changing it to 5
        if key[ord('s')]: # down key
            self.y += dist # move down
        elif key[ord('w')]: # up key
            self.y -= dist # move up
        if key[ord('d')]: # right key
            self.x += dist # move right
        elif key[ord('a')]: # left key
            self.x -= dist # move left
        if(self.x > (WIDTH-35)):
            self.x -= dist
        elif(self.x < 0):
            self.x += dist
        elif(self.y < 0):
            self.y += dist
        elif(self.y > (HEIGHT-35)):
            self.y -= dist

    def draw(self, surface):
        """ Draw on surface """
        # blit yourself at your current position
        surface.blit(self.image, (self.x, self.y))
  
# Define our bgRect object and call super to 
# give it all the properties and methods of pygame.sprite.Sprite 
# Define the class for our bgRect objects 
class bgRect(pygame.sprite.Sprite): 
    def __init__(self): 
        super(bgRect, self).__init__() 
          
        # Define the dimension of the surface 
        # Here we are making bgRects of side 25px 
        self.surf = pygame.Surface((WIDTH, 30)) 
          
        # Define the color of the surface using RGB color coding. 
        self.surf.fill((153, 76, 0)) 
        self.rect = self.surf.get_rect() 

class Static_Obstacle(object):  
    def __init__(self):
        """ The constructor of the class """
        self.img_b = pygame.image.load(os.path.join(img_path, 'bomb.png'))
        self.image = pygame.transform.rotozoom(self.img_b, 0, 0.9)
        # the Player's position
        self.x = 0
        self.y = 0
        
    
    def draw(self, surface):
        """ Draw on surface """
        # blit yourself at your current position
        surface.blit(self.image, (self.x, self.y))

time_elapsed=0
def check_if_time_has_elapsed_in_seconds(seconds):
    global time_elapsed
    if time_elapsed > seconds*1000:
        time_elapsed = 0
        return True
    else:
        return False 

def check_collison(enemyx, enemyy, playerx, playery):
    distance = math.sqrt((math.pow(enemyx - playerx, 2)) + (math.pow(enemyy - playery, 2)))
    if(distance < 40):
        return True
    else:
        return False

def check_collison1(enemyx, enemyy, playerx, playery):
    distance = math.sqrt((math.pow(enemyx - playerx, 2)) + (math.pow(enemyy - playery, 2)))
    if(distance < 60):
        return True
    else:
        return False

def check_collison2(enemyx, enemyy, playerx, playery):
    distance = math.sqrt((math.pow(enemyx - playerx, 2)) + (math.pow(enemyy - playery, 2)))
    if(distance < 30):
        return True
    else:
        return False

def game_over_text():  
    over_text = over_font.render("PLAYER 1 DIES :)", True, (255, 255, 255))
    screen.blit(over_text, (170, 250))
    global p1_hit
    p1_hit=1     # keeping record of collisons
    #print ("hit")

def game_over_text1():  
    #over_text = over_font.render("PLAYER 2 DIES :)", True, (255, 255, 255))
   # screen.blit(over_text, (170, 250))
    #pygame.display.flip()
    #pygame.time.wait(1000)
    global p2_hit
    p2_hit=1
    #print ("hit")

# initialize pygame
  
# instansiate player
player1 = Player1()
player1.x = 400
player1.y = 600
p1_alive=[]         #variable checks if player is to be displayed
p1_alive.append(1)
global p1_just_died
p1_just_died=0
p1_round_complete=0

player2 = Player2()
player2.x = 400
player2.y = 0
p2_alive=[]
p2_alive.append(0)
global p2_just_died
p2_just_died=0
p2_round_complete=0

#create static obstacles
bomb1 = Static_Obstacle()
bomb1.x = 10
bomb1.y = 110

bomb2 = Static_Obstacle()
bomb2.x = 390
bomb2.y = 110

bomb3 = Static_Obstacle()
bomb3.x = 200
bomb3.y = 110

bomb4 = Static_Obstacle()
bomb4.x = 760
bomb4.y = 110

bomb5 = Static_Obstacle()
bomb5.x = 600
bomb5.y = 110

bomb6 = Static_Obstacle()
bomb6.x = 10
bomb6.y = 340

bomb7 = Static_Obstacle()
bomb7.x = 390
bomb7.y = 340

bomb8 = Static_Obstacle()
bomb8.x = 200
bomb8.y = 340

bomb9 = Static_Obstacle()
bomb9.x = 760
bomb9.y = 340

bomb10 = Static_Obstacle()
bomb10.x = 600
bomb10.y = 340

bomb11 = Static_Obstacle()
bomb11.x = 100
bomb11.y = 230

bomb12 = Static_Obstacle()
bomb12.x = 300
bomb12.y = 230

bomb13 = Static_Obstacle()
bomb13.x = 500
bomb13.y = 230

bomb14 = Static_Obstacle()
bomb14.x = 700
bomb14.y = 230

bomb15 = Static_Obstacle()
bomb15.x = 100
bomb15.y = 460

bomb16 = Static_Obstacle()
bomb16.x = 300
bomb16.y = 460

bomb17 = Static_Obstacle()
bomb17.x = 500
bomb17.y = 460

bomb18 = Static_Obstacle()
bomb18.x = 700
bomb18.y = 460

#creating moving obstacle
croc_1_x=0
croc_1_y=30
croc_2_x=200
croc_2_y=150
croc_3_x=280
croc_3_y=260
croc_4_x=520
croc_4_y=380
croc_5_x=715
croc_5_y=490

croc_speed=[]
croc_speed.append(3)
croc_speed.append(3)

clock = pygame.time.Clock()

# instantiate all bgRect objects 
bgRect1 = bgRect() 
bgRect2 = bgRect() 
bgRect3 = bgRect() 
bgRect4 = bgRect()
bgRect5 = bgRect() 
bgRect6 = bgRect()  

dt=clock.tick(1)
  
# Variable to keep our game loop running 
gameOn = True
time_start=pygame.time.get_ticks() 
# Our game loop 
while gameOn: 
    # for loop through the event queue 
    for event in pygame.event.get(): 
          
        # Check for KEYDOWN event 
        if event.type == KEYDOWN: 
              
            # If the Backspace key has been pressed set 
            # running to false to exit the main loop 
            if event.key == K_BACKSPACE: 
                gameOn = False
                  
        # Check for QUIT event 
        elif event.type == QUIT:
            gameOn = False
  
    # Define where the bgRects will appear on the screen 
    # Use blit to draw them on the screen surface 
    screen.fill((0, 200, 255))
    screen.blit(bgRect1.surf, (0, 0)) 
    screen.blit(bgRect2.surf, (0, 110)) 
    screen.blit(bgRect3.surf, (0, 230)) 
    screen.blit(bgRect4.surf, (0, 340))
    screen.blit(bgRect5.surf, (0, 460))
    screen.blit(bgRect6.surf, (0, 570)) 
   
    # having different speed controls for each player

    if p1_alive[0]==1:
        croc_1_x+=croc_speed[0]
        croc_2_x+=croc_speed[0]
        croc_3_x+=croc_speed[0]
        croc_4_x+=croc_speed[0]
        croc_5_x+=croc_speed[0]
    elif p2_alive[0]==1:
        croc_1_x+=croc_speed[1]
        croc_2_x+=croc_speed[1]
        croc_3_x+=croc_speed[1]
        croc_4_x+=croc_speed[1]
        croc_5_x+=croc_speed[1]        

    if croc_1_x>(WIDTH-35):
        croc_1_x=0
    if croc_2_x>(WIDTH-35):
        croc_2_x=0
    if croc_3_x>(WIDTH-35):
        croc_3_x=0
    if croc_4_x>(WIDTH-35):
        croc_4_x=0
    if croc_5_x>(WIDTH-35):
        croc_5_x=0        

    screen.blit(croc_img,(croc_1_x,croc_1_y))
    screen.blit(croc_img,(croc_2_x,croc_2_y))
    screen.blit(croc_img,(croc_3_x,croc_3_y))
    screen.blit(croc_img,(croc_4_x,croc_4_y))
    screen.blit(croc_img,(croc_5_x,croc_5_y))
    
    over_text = over_font_score.render("Player 1: "+str(player1.score), True, (255, 255, 255))
    screen.blit(over_text, (0, 0))

    over_text = over_font_score.render("Player 2: "+str(player2.score), True, (255, 255, 255))
    screen.blit(over_text, (0, 575))

    if p1_alive[0]==1:
        over_text = over_font_score.render(" END ", True, (255, 255, 255))
        screen.blit(over_text, (350, 0))
        over_text = over_font_score.render(" START ", True, (255, 255, 255))
        screen.blit(over_text, (350,575))

    if p2_alive[0]==1:
        over_text = over_font_score.render(" START ", True, (255, 255, 255))
        screen.blit(over_text, (350, 0))
        over_text = over_font_score.render(" END ", True, (255, 255, 255))
        screen.blit(over_text, (350,575))
    
    if p1_alive[0]==1:
        player1.handle_keys()
    if p2_alive[0]==1:
        player2.handle_keys()
    if player1.y==0:
        player1.chk=0
        p1_alive.pop()
        p1_alive.append(0)
        over_text = over_font.render("Level Complete", True, (255, 255, 255))
        screen.blit(over_text, (200, 250))
        pygame.display.flip()
        pygame.time.wait(2500)
       # time_elapsed+=dt
        p1_round_complete=1
        p2_round_complete=0
        #if check_if_time_has_elapsed_in_seconds(30):
        player1.x=400
        player1.y=600
        p2_alive.pop()
        p2_alive.append(1)
        penalty=math.floor((pygame.time.get_ticks()-time_start)/1000)
        player1.score-=penalty
        over_text = over_font.render("Time Penalty = "+str(penalty), True, (255, 255, 255))
        screen.blit(over_text, (150, 400))
        pygame.display.flip()
        pygame.time.wait(2500)
        time_start=pygame.time.get_ticks()

    if p1_alive[0]==1:
        player1.draw(screen)

    if player2.y==565:
        player2.chk=0
        p2_alive.pop()
        p2_alive.append(0)
        #over_text = over_font.render("Level Complete", True, (255, 255, 255))
        #screen.blit(over_text, (200, 250))
        #time_elapsed+=dt
        p2_round_complete=1
        #if check_if_time_has_elapsed_in_seconds(30):
        player2.x=400
        player2.y=0
        #player2.score+=150
            #if player2.score>370:
        #player2.score=(player2.score%370)*370
        penalty=math.floor((pygame.time.get_ticks()-time_start)/1000)
        player2.score-=penalty
        over_text = over_font.render("Time Penalty = "+str(penalty), True, (255, 255, 255))
        screen.blit(over_text, (150, 400))
        pygame.display.flip()
        pygame.time.wait(2500)
        time_start=pygame.time.get_ticks()
    
    if p1_round_complete==1 and p2_round_complete==1: 
        #print ("wot")
        #time_elapsed+=dt
        if player1.score > player2.score:
            over_text = over_font.render("Player 1 Wins", True, (255, 255, 255))
            screen.blit(over_text, (200, 250))
            croc_speed[0]*=2
        else:
            over_text = over_font.render("Player 2 Wins", True, (255, 255, 255))
            screen.blit(over_text, (200, 250)) 
            croc_speed[1]*=2          
        pygame.display.flip()
        pygame.time.wait(2500)
        #if check_if_time_has_elapsed_in_seconds(30):
        p1_alive.pop()
        p1_alive.append(1)
        #croc_speed[0]*=2
        p1_round_complete=0
        p2_round_complete=0
        player1.score=0
        player2.score=0
        time_start=pygame.time.get_ticks()

    if p2_round_complete==1 and p1_round_complete==0:
        #time_elapsed+=dt
        #if check_if_time_has_elapsed_in_seconds(30):      
        if player1.score > player2.score:
            over_text = over_font.render("Player 1 Wins", True, (255, 255, 255))
            screen.blit(over_text, (200, 250))
            croc_speed[0]*=2
        else:
            over_text = over_font.render("Player 2 Wins", True, (255, 255, 255))
            screen.blit(over_text, (200, 250)) 
            croc_speed[1]*=2
        pygame.display.flip()
        pygame.time.wait(2500)
        p1_alive[0]=1
        p2_round_complete=0
        p1_just_died=0
        p1_hit=0
        player1.score=0
        player2.score=0
        player1.chk=0
        time_start=pygame.time.get_ticks()
        #croc_speed[1]*=2

    if p1_round_complete==1 and p2_just_died==1:
        #time_elapsed+=dt
        #if check_if_time_has_elapsed_in_seconds(30):
        if player1.score > player2.score:
            over_text = over_font.render("Player 1 Wins", True, (255, 255, 255))
            screen.blit(over_text, (200, 250))
            croc_speed[0]*=2
        else:
            over_text = over_font.render("Player 2 Wins", True, (255, 255, 255))
            screen.blit(over_text, (200, 250)) 
            croc_speed[1]*=2
        pygame.display.flip()
        pygame.time.wait(2500)
        p1_alive[0]=1
        p1_round_complete=0
        p2_just_died=0
        p2_hit=0
        player1.score=0
        player2.score=0
        player2.chk=0  #resetting game
        #croc_speed[0]*=2


    if (p2_alive[0]==1):
        player2.draw(screen)

    bomb1.draw(screen)
    bomb2.draw(screen)
    bomb3.draw(screen)
    bomb4.draw(screen)
    bomb5.draw(screen)
    bomb6.draw(screen)
    bomb7.draw(screen)
    bomb8.draw(screen)
    bomb9.draw(screen)
    bomb10.draw(screen)
    bomb11.draw(screen)
    bomb12.draw(screen)
    bomb13.draw(screen)
    bomb14.draw(screen)
    bomb15.draw(screen)
    bomb16.draw(screen)
    bomb17.draw(screen)
    bomb18.draw(screen)

    collision1 = check_collison(bomb1.x,bomb1.y,player1.x, player1.y)
    if collision1:
        game_over_text()
        p1_alive.pop()
        p1_alive.append(0)

    collision2 = check_collison(bomb2.x,bomb2.y,player1.x, player1.y)
    if collision2:
        game_over_text()
        p1_alive.pop()
        p1_alive.append(0)

    collision3 = check_collison(bomb3.x,bomb3.y,player1.x, player1.y)
    if collision3:
        game_over_text()          
        p1_alive.pop()
        p1_alive.append(0)

    collision4 = check_collison(bomb4.x,bomb4.y,player1.x, player1.y)
    if collision4:
        game_over_text()
        p1_alive.pop()
        p1_alive.append(0)

    collision5 = check_collison(bomb5.x,bomb5.y,player1.x, player1.y)
    if collision5:
        game_over_text()
        p1_alive.pop()
        p1_alive.append(0)       

    collision6 = check_collison(bomb6.x,bomb6.y,player1.x, player1.y)
    if collision6:
        game_over_text()
        p1_alive.pop()
        p1_alive.append(0)

    collision7 = check_collison(bomb7.x,bomb7.y,player1.x, player1.y)
    if collision7:
        game_over_text()
        p1_alive.pop()
        p1_alive.append(0)

    collision8 = check_collison(bomb8.x,bomb8.y,player1.x, player1.y)
    if collision8:
        game_over_text()          
        p1_alive.pop()
        p1_alive.append(0)

    collision9 = check_collison(bomb9.x,bomb9.y,player1.x, player1.y)
    if collision9:
        game_over_text()
        p1_alive.pop()
        p1_alive.append(0)

    collision10 = check_collison(bomb10.x,bomb10.y,player1.x, player1.y)
    if collision10:
        game_over_text()       
        p1_alive.pop()
        p1_alive.append(0)

    collision11 = check_collison(bomb11.x,bomb11.y,player1.x, player1.y)
    if collision11:
        game_over_text()
        p1_alive.pop()
        p1_alive.append(0)

    collision12 = check_collison(bomb12.x,bomb12.y,player1.x, player1.y)
    if collision12:
        game_over_text()
        p1_alive.pop()
        p1_alive.append(0)

    collision13 = check_collison(bomb13.x,bomb13.y,player1.x, player1.y)
    if collision13:
        game_over_text()          
        p1_alive.pop()
        p1_alive.append(0)

    collision14 = check_collison(bomb14.x,bomb14.y,player1.x, player1.y)
    if collision14:
        game_over_text()
        p1_alive.pop()
        p1_alive.append(0)

    collision15 = check_collison(bomb15.x,bomb15.y,player1.x, player1.y)
    if collision15:
        game_over_text()      
        p1_alive.pop()
        p1_alive.append(0)

    collision16 = check_collison(bomb16.x,bomb16.y,player1.x, player1.y)
    if collision16:
        game_over_text()          
        p1_alive.pop()
        p1_alive.append(0)

    collision17 = check_collison(bomb17.x,bomb17.y,player1.x, player1.y)
    if collision17:
        game_over_text()
        p1_alive.pop()
        p1_alive.append(0)

    collision18 = check_collison(bomb18.x,bomb18.y,player1.x, player1.y)
    if collision18:
        game_over_text() 
        p1_alive.pop()
        p1_alive.append(0)  

    collision19 = check_collison1(croc_1_x,croc_1_y,player1.x, player1.y)
    if collision19:
        game_over_text() 
        p1_alive.pop()
        p1_alive.append(0)

    collision20 = check_collison1(croc_2_x,croc_2_y,player1.x, player1.y)
    if collision20:
        game_over_text() 
        p1_alive.pop()
        p1_alive.append(0)

    collision21 = check_collison1(croc_3_x,croc_3_y,player1.x, player1.y)
    if collision21:
        game_over_text() 
        p1_alive.pop()
        p1_alive.append(0)

    collision22 = check_collison1(croc_4_x,croc_4_y,player1.x, player1.y)
    if collision22:
        game_over_text() 
        p1_alive.pop()
        p1_alive.append(0)

    collision23 = check_collison1(croc_5_x,croc_5_y,player1.x, player1.y)
    if collision23:
        game_over_text() 
        p1_alive.pop()
        p1_alive.append(0)
    
    if ((p1_hit==1) and(p1_just_died==0)):
        time_elapsed+=dt
        if check_if_time_has_elapsed_in_seconds(30):
            global p1_just_died
            player1.x=400
            player1.y=600
            p1_just_died=1
            p2_alive.pop()
            p2_alive.append(1)

    collision1 = check_collison(bomb1.x,bomb1.y,player2.x, player2.y)
    if collision1:
        game_over_text1()
        p2_alive.pop()
        p2_alive.append(0)

    collision2 = check_collison(bomb2.x,bomb2.y,player2.x, player2.y)
    if collision2:
        game_over_text1()
        p2_alive.pop()
        p2_alive.append(0)

    collision3 = check_collison(bomb3.x,bomb3.y,player2.x, player2.y)
    if collision3:
        game_over_text1()          
        p2_alive.pop()
        p2_alive.append(0)

    collision4 = check_collison(bomb4.x,bomb4.y,player2.x, player2.y)
    if collision4:
        game_over_text1()
        p2_alive.pop()
        p2_alive.append(0)

    collision5 = check_collison(bomb5.x,bomb5.y,player2.x, player2.y)
    if collision5:
        game_over_text1()
        p2_alive.pop()
        p2_alive.append(0)       

    collision6 = check_collison(bomb6.x,bomb6.y,player2.x, player2.y)
    if collision6:
        game_over_text1()
        p2_alive.pop()
        p2_alive.append(0)

    collision7 = check_collison(bomb7.x,bomb7.y,player2.x, player2.y)
    if collision7:
        game_over_text1()
        p2_alive.pop()
        p2_alive.append(0)

    collision8 = check_collison(bomb8.x,bomb8.y,player2.x, player2.y)
    if collision8:
        game_over_text1()          
        p2_alive.pop()
        p2_alive.append(0)

    collision9 = check_collison(bomb9.x,bomb9.y,player2.x, player2.y)
    if collision9:
        game_over_text1()
        p2_alive.pop()
        p2_alive.append(0)

    collision10 = check_collison(bomb10.x,bomb10.y,player2.x, player2.y)
    if collision10:
        game_over_text1()       
        p2_alive.pop()
        p2_alive.append(0)

    collision11 = check_collison(bomb11.x,bomb11.y,player2.x, player2.y)
    if collision11:
        game_over_text1()
        p2_alive.pop()
        p2_alive.append(0)

    collision12 = check_collison(bomb12.x,bomb12.y,player2.x, player2.y)
    if collision12:
        game_over_text1()
        p2_alive.pop()
        p2_alive.append(0)

    collision13 = check_collison(bomb13.x,bomb13.y,player2.x, player2.y)
    if collision13:
        game_over_text1()          
        p2_alive.pop()
        p2_alive.append(0)

    collision14 = check_collison(bomb14.x,bomb14.y,player2.x, player2.y)
    if collision14:
        game_over_text1()
        p2_alive.pop()
        p2_alive.append(0)

    collision15 = check_collison(bomb15.x,bomb15.y,player2.x, player2.y)
    if collision15:
        game_over_text1()      
        p2_alive.pop()
        p2_alive.append(0)

    collision16 = check_collison(bomb16.x,bomb16.y,player2.x, player2.y)
    if collision16:
        game_over_text1()          
        p2_alive.pop()
        p2_alive.append(0)

    collision17 = check_collison(bomb17.x,bomb17.y,player2.x, player2.y)
    if collision17:
        game_over_text1()
        p2_alive.pop()
        p2_alive.append(0)

    collision18 = check_collison(bomb18.x,bomb18.y,player2.x, player2.y)
    if collision18:
        game_over_text1() 
        p2_alive.pop()
        p2_alive.append(0)  

    collision19 = check_collison2(croc_1_x,croc_1_y,player2.x, player2.y)
    if collision19:
        game_over_text1() 
        p2_alive.pop()
        p2_alive.append(0)

    collision20 = check_collison2(croc_2_x,croc_2_y,player2.x, player2.y)
    if collision20:
        game_over_text1() 
        p2_alive.pop()
        p2_alive.append(0)

    collision21 = check_collison2(croc_3_x,croc_3_y,player2.x, player2.y)
    if collision21:
        game_over_text1() 
        p2_alive.pop()
        p2_alive.append(0)

    collision22 = check_collison2(croc_4_x,croc_4_y,player2.x, player2.y)
    if collision22:
        game_over_text1() 
        p2_alive.pop()
        p2_alive.append(0)

    collision23 = check_collison2(croc_5_x,croc_5_y,player2.x, player2.y)
    if collision23:
        game_over_text1() 
        p2_alive.pop()
        p2_alive.append(0)
    
    if ((p2_hit==1) and(p2_just_died==0)):
        time_elapsed+=dt
        if check_if_time_has_elapsed_in_seconds(30):
            global p2_just_died
            player2.x=400
            player2.y=0
            p2_just_died=1
            #p2_alive.pop()
            #p2_alive.append(1)

    if p1_just_died==1 and p2_just_died==1:
        penalty=math.floor((pygame.time.get_ticks()-time_start)/1000)
        player1.score-=penalty
        time_start=pygame.time.get_ticks()-time_start
       # over_text = over_font.render("Time Penalty 1 = "+str(penalty), True, (255, 255, 255))
        #screen.blit(over_text, (120, 400))
        #pygame.display.flip()
        #pygame.time.wait(2500)
        penalty=math.floor((pygame.time.get_ticks()-time_start)/1000)
        player2.score-=penalty
        time_start=pygame.time.get_ticks()-time_start
        '''over_text = over_font.render("Time Penalty 2 = "+str(penalty), True, (255, 255, 255))
        screen.blit(over_text, (120, 400))
        pygame.display.flip()
        pygame.time.wait(2500)'''
        if (player2.score>player1.score):
            over_text = over_font.render("Player 2 Wins", True, (255, 255, 255))
            screen.blit(over_text, (200, 250))
            pygame.display.flip()
            pygame.time.wait(2500)
            croc_speed[1]*=2
        else:
            over_text = over_font.render("Player 1 Wins", True, (255, 255, 255))
            screen.blit(over_text, (200, 250))
            pygame.display.flip()
            pygame.time.wait(2500)
            croc_speed[0]*=2
        p1_alive[0]=1
        p1_hit=0
        p1_just_died=0
        player1.chk=0
        player1.score=0
        p2_hit=0
        p2_just_died=0
        player2.chk=0
        player2.score=0   
        time_start=pygame.time.get_ticks()          
           

    if player1.y < 480 and player1.chk == 0:
        player1.score += 10
        player1.chk += 1
    elif player1.y < 460 and player1.chk == 1:
        player1.score += 5
        player1.chk += 1
    elif player1.y < 360 and player1.chk == 2:
        player1.score += 10
        player1.chk += 1
    elif player1.y < 340 and player1.chk == 3:
        player1.score += 5
        player1.chk += 1
    elif player1.y < 250 and player1.chk == 4:
        player1.score += 10
        player1.chk += 1
    elif player1.y < 230 and player1.chk == 5:
        player1.score += 5
        player1.chk += 1
    elif player1.y < 130 and player1.chk == 6:
        player1.score += 10
        player1.chk += 1
    elif player1.y < 110 and player1.chk == 7:
        player1.score += 5
        player1.chk += 1
    elif player1.y < 20 and player1.chk == 8:
        player1.score += 10
        player1.chk += 1


    if player2.y > 480 and player2.chk == 8:
        player2.score += 10
        player2.chk += 1
    elif player2.y > 460 and player2.chk == 7:
        player2.score += 5
        player2.chk += 1
    elif player2.y > 360 and player2.chk == 6:
        player2.score += 10
        player2.chk += 1
    elif player2.y > 340 and player2.chk == 5:
        player2.score += 5
        player2.chk += 1
    elif player2.y > 250 and player2.chk == 4:
        player2.score += 10
        player2.chk += 1
    elif player2.y > 230 and player2.chk == 3:
        player2.score += 5
        player2.chk += 1
    elif player2.y > 130 and player2.chk == 2:
        player2.score += 10
        player2.chk += 1
    elif player2.y > 110 and player2.chk == 1:
        player2.score += 5
        player2.chk += 1
    elif player2.y > 20 and player2.chk == 0:
        player2.score += 10
        player2.chk += 1
    #print (p2_just_died)
    #print (p1_hit)
            

    # Update the display using flip 
    pygame.display.flip() 
    clock.tick(40)
