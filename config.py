import pygame 
import os  
import sys
import math
import config

from pygame.locals import *

pygame.init()

pygame.mixer.music.load('Defense Line.mp3')
pygame.mixer.music.play(-1)

over_font = pygame.font.Font('freesansbold.ttf', 64)
over_font_score = pygame.font.Font('freesansbold.ttf', 20)

WIDTH = 800
HEIGHT = 600

curr_path = os.path.dirname(__file__)
img_path = os.path.join(curr_path, 'images')

# Define the dimensions of screen object 
screen = pygame.display.set_mode((WIDTH, HEIGHT))

#defining moving obstacle
croc_imgb=pygame.image.load(os.path.join(img_path, 'fish.png'))
croc_img=pygame.transform.rotozoom(croc_imgb, 0, 2.5)
